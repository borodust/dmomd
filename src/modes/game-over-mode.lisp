(in-package #:dmomd)


;;;; GAME-OVER MODE
;;;;----------------------------------------------------------------------------
(defclass game-over-mode (mode) ()
  (:default-initargs
   :mode-music nil))


;;;;-----------------------------------------------------------------------------
(defmethod mode-init ((m game-over-mode))
  (add-timer (+ (now) 5) (lambda () (add-fade :duration 1.5 :ease :in)))
  (play-sound :snd-lose))

;;;;-----------------------------------------------------------------------------
(defmethod mode-act ((m game-over-mode))
  (when (< 6.5 (elapsed-time m))
    (switch-mode 'title-mode)))

;;;-----------------------------------------------------------------------------
(defmethod mode-draw ((m game-over-mode))
  (draw-sprite (get-resource '(:img :battle :player :dying))
               (vector->vec2 #(500 175)))
  (draw-text "GAME-OVER"
             (tgk:vec2 480 420)
             :fill-color (tgk:vec4 1 0 0 1)
             :font (font :quikhand 80)))

;;;;-----------------------------------------------------------------------------
(defmethod cleanup ((m game-over-mode)))
