(in-package #:dmomd)

;;;;----------------------------------------------------------------------------
;;;; MODE
;;;;----------------------------------------------------------------------------
(defclass mode ()
  ((game :accessor mode-game)
   (init-time :reader init-time
              :documentation "initialized by (now) on mode-init")
   (music-playing :accessor music-playing
                  :initform nil
                  :documentation "Each mode needs to set this variable in the class deklaration.")
   (mode-music :accessor mode-music
                 :initarg :mode-music
                 :initform (error "Mode needs to have a default music. Use NIL for none."))
   (bound-buttons :accessor bound-buttons
                  :initarg :bound-buttons
                  :initform nil)
   (root-menu
    :accessor root-menu
    :initarg :root-menu
    :initform nil)
   (active-menu
    :accessor active-menu
    :initarg :active-menu
    :initform nil)))

(defmethod mode-act ((m mode)))
(defmethod mode-draw ((m mode)))

(defmethod mode-init :before ((m mode))
  (add-fade :duration .5 :ease :out)
  (setf (slot-value m 'init-time) (now))
  (when *play-mode-music*
    (if *in-boss-battle-p*
        (add-timer (+ (now) (- *boss-battle-enter-delay* 1))
                   (lambda () (play-mode-music m)))
     (play-mode-music m)))
  (create-interface m)
  (unless *dont-bind-buttons*
   (bind-buttons m)))

(defmethod mode-init ((m mode)))

(defmethod create-interface (mode)
  "Creates all necessary interace objects and sets up the appropriate keybinding for using the interface."
  nil)

(defmethod bind-buttons ((m mode))
  (format t "Each mode should implement their own `bind-buttons' method. Method `bind-button' called with ~A." m))

(defmethod unbind-buttons ((m mode))
  (loop :for button :in (bound-buttons m) :do
       (tgk:bind-button button :pressed nil)
       (tgk:bind-button button :released nil)
       (tgk:bind-button button :repeating nil)))

(defmethod select-menu-next (m)
  (print "select-menu-next called with unknown argument."))

(defmethod select-menu-previous (m)
  (print "select-menu-previous called with unknown argument."))

(defmethod switch-mode (mode-class &rest keys)
  (cleanup *mode*)  ; do everything necessary to leave the mode in a clean state
  (let ((mode (apply #'make-instance mode-class keys)))
    (setf *mode* mode)
    (mode-init *mode*)))

(defmethod play-mode-music (mode)
  (unless (music-playing mode)
    (play-sound (mode-music mode) :looped-p t)
    (setf (music-playing mode) t)))

(defun stop-mode-music (&optional (m *mode*))
  (when (and (mode-music m) (music-playing m))
    (stop-sound (mode-music m))
    (setf (music-playing m) nil)))

(defmethod elapsed-time ((m mode))
  "How much time has passed since we've entered the mode?"
  (- (now) (init-time m)))

;;; Interface

(defmethod select-menu-next :before ((m mode))
  (when (eq *current-attacker* *player*)
    (play-sound :snd-button-change)))

(defmethod select-menu-next ((m mode))
  "Set the current menu item to the item to the right and play a button click sound."
  (with-accessors ((active-menu active-menu))
      m
    (when (and (child-menu active-menu)
               (not (info-menu-p (child-menu active-menu))))
      (setf (menu-visibility active-menu) :inactive)
      (setf active-menu (child-menu active-menu))
      (setf (menu-visibility active-menu) :active))))

(defmethod select-menu-previous :before ((m mode))
  (when (eq *current-attacker* *player*)
   (play-sound :snd-button-change)))

(defmethod select-menu-previous ((m mode))
  "Set the current menu item to the item to the left"
  (with-accessors ((active-menu active-menu))
      m
    (when (menu-parent-menu active-menu)
      (setf (menu-visibility active-menu) :inactive)
      (setf active-menu (menu-parent-menu active-menu))
      (setf (menu-visibility active-menu) :active))))

(defmethod disable-mode-music ((m mode))
  (setf *play-mode-music* nil)
  (stop-mode-music m))

(defmethod enable-mode-music ((m mode))
  (setf *play-mode-music* t)
  (play-mode-music m))

(defmethod toggle-mode-music ((m mode))
  (if *play-mode-music* ; mode music is playing
      (disable-mode-music m)
      (enable-mode-music  m)))

(defmethod mode-draw :after ((m mode))
  (process-drawing-queue)
  (process-animation-queue)
  (process-fades))

;;;;----------------------------------------------------------------------------
(defmethod cleanup (m)) ; we need this one for the initial switch to 'title-mode, when *mode* is still NIL.

(defmethod cleanup :before ((m mode))
  "This method is implemented for each mode and defined specific cleanup behaviour."
  ;; (clear-fades)
  ;; (clear drawing-instructions)
  (stop-sounds)
  (stop-mode-music m)
  (unbind-buttons m))

(defmethod cleanup ((m mode))
  "This method is implemented for each mode and defined specific cleanup behaviour.")

(defun stop-sounds (&optional (sounds *playing-sounds*))
  (dolist (s sounds)
    (stop-sound s)
    (pop sounds)))


(defun mode-younger-than-p (time &optional (m *mode*))
  (> (+ (init-time m) time)
     (now)))
