(in-package #:dmomd)

(defun draw-win-credits ()
  (progn
        (draw-image *window-bottom-left-corner*
                    :img-credits-background-2-bg)
        (draw-string-list *win-credits-draw-pos*
                          20 ; gap
                          (hexcolor "#272C43")
                          (font :consola 30) ; font
                          30 ; font height
                          '("Programming - decent-username,"
                            "              moldybits"
                            ""
                            ""
                            "Images - decent-username"
                            ""
                            ""
                            "Title Music - Public Domain"
                            "Dungeon Music - Brandon Morris"
                            "Battle Music - PlayOnLoop"
                            "Boss Battle Music - Scrabbit"
                            "Sound Effects - Kenney & others"
                            ""
                            ""
                            "Main Font - Joanne Taylor"
                            "Consola Font - wmk69"
                            ""
                            ""
                            "       SPECIAL THANKS TO"
                            ""
                            "MFIANO - dungen library"
                            ""
                            "BORODUST - trivial-gamekit library"
                            ""
                            "PIXEL_OUTLAW - trivial-gamekit template"
                            ""
                            ""
                            "             AND"
                            "         #LISPGAMES"))
        (draw-image *window-bottom-left-corner*
                    :img-credits-background-2-fg)
        (setf *win-credits-draw-pos* (tgk:add *win-credits-draw-pos*
                                              (tgk:vec2 0 .5)))))

(defun draw-loose-credits ()
  (draw-string-list *loose-credits-draw-pos*
                          20 ; gap
                          (hexcolor "#851818")
                          (font :consola 30) ; font
                          30 ; font height
                          '("Programming - decent-username,"
                            "              moldybits"
                            ""
                            ""
                            "Images - decent-username"
                            ""
                            ""
                            "Title Music - Public Domain"
                            "Dungeon Music - Brandon Morris"
                            "Battle Music - PlayOnLoop"
                            "Boss Battle Music - Scrabbit"
                            "Sound Effects - Kenney & others"
                            ""
                            ""
                            "Main Font - Joanne Taylor"
                            "Consola Font - wmk69"
                            ""
                            ""
                            "       SPECIAL THANKS TO"
                            ""
                            "MFIANO - dungen library"
                            ""
                            "BORODUST - trivial-gamekit library"
                            ""
                            "PIXEL_OUTLAW - trivial-gamekit template"
                            ""
                            ""
                            "             AND"
                            "         #LISPGAMES")))
