;;;; dmomd.lisp
(in-package #:dmomd)



;;;-----------------------------------------------------------------------------
;;; Initializes the entire game
;;;-----------------------------------------------------------------------------
(defun initialize-game ()
  (setf *player-won* nil)
  (setf *dungeon-rooms* (make-hash-table :test #'equalp))  ; used by make-dungeon
  (setf *dungeon-empty-rooms* nil)                         ; used by make-dungeon
  (make-dungen)                                            ; make-dungeon
  (setf *dungeon-camera* (make-camera 0
                                      0
                                      (elt *internal-screen-size* 0)
                                      (elt *internal-screen-size* 1)))
  (setf *rogue-mode-items* nil)
  (setf *rogue-mode-inventory-menu* nil)
  (setf *settings-menu* nil)
  (setf *items* (make-hash-table))
  (create-reference-items *items*)

  (populate-poly-edges *grid*)  ; necessary for the LOS algorithm
  (setf *seen-cells* nil)
  (setf *last-safe-cell* nil)
  (setf *timers* nil)
  (setf *in-battle* nil)
  (setf *player-won* nil)
  (setf *enemies* nil)
  (create-all-animations)
  ;; player should be created here
  (setf *player*
        (make-instance 'player
                       :hp-max 70
                       :mp-max 8
                       :vision-radius 3.45
                       :vulnerabilities (list :poison)
                       :resistances (list :fire :lightning)))

  (setf *demon* (make-demon))
  ;; The order in which these spawn functions are called matters a lot!!!
  (spawn-player)
  (spawn-treasure)
  (dotimes (i 2)
      (spawn-enemies))
  ;; spawn potions
  (dotimes (i 3)
    (spawn-items)))


;;;----------------------------------------------------------------------------
;;; GAMEKIT
;;;----------------------------------------------------------------------------

;;; define game
(tgk:defgame tgk-game () ()
             (:viewport-width  (elt *window-size* 0))
             (:viewport-height (elt *window-size* 1))
             (:act-rate  60)
             (:draw-rate 60)
             (:viewport-title "Decent Magician Of the Moldy Dungeon")
             (:prepare-resources nil))

(defmethod gamekit:notice-resources ((this tgk-game) &rest resource-names)
  (push (first resource-names) *loaded-resources*)
  (setf *loading-resources* (delete (first resource-names) *loading-resources*))
  (if (= (length *loaded-resources*) *number-of-registered-resources*)
      (switch-mode 'title-mode)))


(let (fonts)
  (defun font (resource-name height)    ; at least i think it's height?
    (let ((tmp (assoc (list resource-name height) fonts :test #'equal)))
      (if tmp
          (cdr tmp)
          ;; it'd be nice if the condition was more specific than simple-error.
          (let ((f (handler-case (tgk:make-font resource-name height) (simple-error () nil))))
            (if f
                (cdar (push (cons (list resource-name height) f)
                            fonts))
                tgk::*font*))))))

;;; post initialize
;;;-----------------------------------------------------------------------------
(defmethod tgk:post-initialize ((o tgk-game))
  (setf *loaded-resources* nil)
  (setf *number-of-registered-resources* 0)
  (switch-mode 'loading-mode)
  (prepare-resources)
  (let ((mouse-position (tgk:vec2 0 0)))
    (defun mouse-position ()
      mouse-position)
    (tgk:bind-cursor (lambda (x y) (setf mouse-position (tgk:vec2 x y))))))

;;; act
;;;-----------------------------------------------------------------------------
(defmethod tgk:act ((o tgk-game))
  (process-timers)
  (mode-act *mode*))

;;; draw
;;;-----------------------------------------------------------------------------
(defmethod tgk:draw ((o tgk-game))
  (clear-screen)
  (mode-draw *mode*))

;;;----------------------------------------------------------------------------
;;; exported functions
;;;----------------------------------------------------------------------------
(defun start ()
  (tgk:start 'tgk-game))

(defun stop ()
  (tgk:stop))

(defun restart-game ()
  (stop)
  (start))
