(in-package #:dmomd)


;;;; POSITION
;;;; --------------------------------------------------------------------------------
;;;; This kind of class has a name in Keene's book.  What was it?
(defclass position-class ()
  ((x :accessor position-x :initarg :x)
   (y :accessor position-y :initarg :y)))


(defmethod print-object ((p position-class) stream)
  (format stream "#~S(~S ~S)"
          (type-of p)
          (position-x p) ; if bound
          (position-y p))) ; if bound


(defclass dungeon-cell-position (position-class) ())
(defclass dungeon-pixel-position (position-class) ())
(defclass dungeon-view-pixel-position (position-class) ())

(defmethod dungeon-cell-position ((p dungeon-cell-position)) p) ; coordinates of a dungeon cell
(defmethod dungeon-pixel-position ((p dungeon-pixel-position)) p) ; dungeon position in pixels

;;; we need to store the camera object within the position objects that use it.
(defmethod dungeon-view-pixel-position ((p dungeon-view-pixel-position)) p)

;;;------------------------------------------------------------------------------
;;; dcp->dpp->dvpp
;;;  \_________^
(defmethod dungeon-pixel-position ((p dungeon-cell-position))
   (make-instance 'dungeon-pixel-position
                 :x (* (position-x p) (elt *dungeon-cell-size* 0))
                 :y (* (position-y p) (elt *dungeon-cell-size* 1))))

;;; dvpp->dpp->dcp
;;;  \__________^
(defmethod dungeon-pixel-position ((p dungeon-view-pixel-position))
  (make-instance 'dungeon-pixel-position
                 :x (+ (position-x p) (camera-bottom-left-x *dungeon-camera*))
                 :y (+ (position-y p) (camera-bottom-left-y *dungeon-camera*))))


;;;------------------------------------------------------------------------------
(defmethod dungeon-view-pixel-position ((p dungeon-pixel-position))
  (make-instance 'dungeon-view-pixel-position
                 :x (- (position-x p) (camera-bottom-left-x *dungeon-camera*))
                 :y (- (position-y p) (camera-bottom-left-y *dungeon-camera*))))

(defmethod dungeon-view-pixel-position ((p dungeon-cell-position))
  (dungeon-view-pixel-position (dungeon-pixel-position p)))


(defmethod dungeon-cell-position ((p dungeon-pixel-position))
  (make-instance 'dungeon-cell-position
                 :x (floor (/ (position-x p) (elt *dungeon-cell-size* 0)))
                 :y (floor (/ (position-y p) (elt *dungeon-cell-size* 1)))))

(defmethod dungeon-cell-position ((p dungeon-view-pixel-position))
  (dungeon-cell-position (dungeon-pixel-position p)))

(defun new-position (from to)
  (unless *in-battle*
   (let ((target-cell (grid-seq-ref to *grid*)))
     (cond
       ;;------------------------------------------------------------
       ((and (hellfire-door-p target-cell)
             (not (has-item-p :hellfire-key *player*)))
        (play-sound :snd-hellfire-door-locked)
        (draw-door-locked-notification)
        ;; don't move
        from)
       ;;------------------------------------------------------------
       ((and (hellfire-door-p target-cell)
             (has-item-p :hellfire-key *player*))
        (play-sound :snd-hellfire-door-open)
        (setf *in-battle* t)
        (add-timer (+ (now) .4) (lambda () (setf *in-battle* nil) (teleport-to! to)))
        from)
       ((cell-has-door-p target-cell)
        (play-sound :snd-door-open)
        to)
       ;;------------------------------------------------------------
       (t (if (cell-wall-p (grid-seq-ref to))
              from
              to))))))

(defun new-position/rel (from to)
  (let ((to (vector (+ (elt to 0) (elt from 0))
                    (+ (elt to 1) (elt from 1)))))
    ;; check for wall or locked hellfire-door
    (new-position from to)))

(defun pixel-position->cell-position (pixel-position)
  (seq-floor (seq/ pixel-position *dungeon-cell-size*)))

(defun glob-pos-from-pos-rel-to-player (cell &optional (player *player*))
  "`cell' is a 2D vector
Returns: the global position of `cell'."
  (let* ((temp-pos (seq+ cell (dungeon-pos player)))
         (result-x (a:clamp (elt temp-pos 0) 0 (1- *dungeon-width*)))
         (result-y (a:clamp (elt temp-pos 1) 0 (1- *dungeon-height*))))
    (vector result-x result-y)))

(defun cell-position->pixel/bottom-left (position)
  "Calculates the position in pixels."
  (seq* *dungeon-cell-size* position))

(defun cell-position->pixel/center (position)
  (seq+ (seq/ *dungeon-cell-size* '(2 2))
        (seq* *dungeon-cell-size* position)))


(defun draw-door-locked-notification ()
  (cond ((or (inside-drawing-queue-p :door-locked-notification-background)
             (inside-drawing-queue-p :door-locked-notification-text))
         (refresh-drawing-timer '(:door-locked-notification-background
                                  :door-locked-notification-text)))
        (t (draw-for (1.9 :door-locked-notification-text)
             `(gamekit:draw-text
               "Looks like I need a special key"
               ,(tgk:subt *window-center* (tgk:vec2 250 80))
               :fill-color ,(hexcolor "#ff0000")
               :font ,(font :quikhand 45)))
           (draw-for (1.9 :door-locked-notification-background)
             `(tgk:draw-rect
               ,(tgk:subt *window-center* (tgk:vec2 260 85))
               455
               45
               :fill-paint ,(alpha-color .8 (hexcolor "#441404"))
               :rounding .5)))))

(defun region-of (pos)
  (cell-region-id (grid-seq-ref pos)))
